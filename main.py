from translator import translation
from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def application():
    if request.method == 'POST':
        user_input = request.form.get('input-text')
        mcode_string = translation(user_input)
        return render_template('index.html', mcode_string=mcode_string, user_input=user_input)
    return render_template('index.html')

# user_input = input("What would you like to convert to Morse Code?\n> ")

while __name__ == '__main__':
    app.run(debug=True)