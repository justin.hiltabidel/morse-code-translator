# Morse Code Translator



## Name
Morse Code Translator

## Description
This project leverages Python's Flask library to generate an easy to use Morse Code Translator, allowing the user to provide a string of text and output the corresponding Morse code.

## Installation
This was built using Python 3.10.4, Flask 2.2.2, and Jinja 3.1.2 versions

## Support
This was developed to evidence work within the Python and Flask frameworks and is not supported for additional aid should issues arise

## Roadmap
No future plans exist at this time

## License
This is copyrighted by me as of 2/19/2023; however, feel free to utilize the toolset as needed.  My only ask is that you cite me as the developer with a link back to my gitlab account.

## Project status
This project is considered complete.
